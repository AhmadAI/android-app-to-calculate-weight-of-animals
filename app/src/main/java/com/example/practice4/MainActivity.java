package com.example.practice4;

import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;

import androidx.core.view.WindowCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.practice4.databinding.ActivityMainBinding;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Button btn;
    TextView text6;
    EditText etext1, etext2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn = findViewById(R.id.btn);
        text6 = findViewById(R.id.text6);
        etext1 = findViewById(R.id.etext1);
        etext2 = findViewById(R.id.etext2);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double girth =Double.parseDouble(etext1.getText().toString());
                double length =Double.parseDouble(etext2.getText().toString());

                double weight = (Math.pow(girth,2)*length)/300;

                text6.setText(weight+"lbs");
            }
        });

    }
}